import { Component, OnInit } from '@angular/core';
import { userObj } from 'src/app/user';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList: userObj [];
  Today = new Date();

  constructor() {
    this.userList = [];
  }

  ngOnInit(): void {
    debugger;
    const records = localStorage.getItem('userList');
    if (records !== null){
      this.userList = JSON.parse(records);
    }
  }

  delete(id: any) {
    debugger;
    if(confirm("Are you sure to delete the value ")) {
    const oldRecords = localStorage.getItem('userList');
      if (oldRecords !== null){
        const userList = JSON.parse(oldRecords);
        userList.splice(userList.findIndex((a: any)=>a.userId == id),1);
        localStorage.setItem('userList', JSON.stringify(userList));
      }
      const records = localStorage.getItem('userList');
      if (records !== null){
        this.userList = JSON.parse(records);
      }
    }
  }
  reMltb() {
    localStorage.removeItem('userList');
  }


}
