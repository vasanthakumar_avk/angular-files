import { Component, OnInit } from '@angular/core';
import { userObj } from 'src/app/user';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  userObj: userObj;
  //Validation
  form: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.maxLength(20),Validators.minLength(3), Validators.pattern('^\s*([a-zA-Z]+\s?\.?[a-zA-Z]+)+$')]),
    userage: new FormControl('', [Validators.required, Validators.maxLength(3), Validators.minLength(2), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    usergender: new FormControl('', [Validators.required]),
    
  });
  submitted = false;

  constructor(private router: Router, private formBuilder: FormBuilder) {
    this.userObj = new userObj();
  }

  //Validation
  ngOnInit(): void {

  }

  //Validation
  get username() {
    return this.form.get('username');
  }
  get userage() {
    return this.form.get('userage');
  }
  get usergender() {
    return this.form.get('usergender');
  }

  //LocalStorage
  getNewUserId() {
    const oldRecords = localStorage.getItem('userList');
    if (oldRecords !== null) {
      const userList = JSON.parse(oldRecords);
      return userList.length + 1;
    }else {
      return 1;
    }
  }

  getNewnUserId() {
    const oldnRecords = localStorage.getItem('usernList');
    if (oldnRecords !== null) {
      const usernList = JSON.parse(oldnRecords);
      return usernList.length + 1;
    }else {
      return 1;
    }
  }
  

  //Valide & LocalStroage
  saveUser() {
  
    if(localStorage.getItem("userList") === null) {
      localStorage.setItem('userList', '[]');
    }
    if(localStorage.getItem("usernList") === null) {
      localStorage.setItem('usernList', '[]');
    }


    if(this.userObj.usergender === true) {
      debugger;
      const latestId = this.getNewUserId();
      this.userObj.userId = latestId;
      const oldRecords = localStorage.getItem('userList');

      this.submitted = true;

      if (this.form.invalid) {
        return;
      }
      if (oldRecords !== null){
        const userList = JSON.parse(oldRecords);
        const dupMname = <HTMLInputElement> document.getElementById("name");
        const dupMage = <HTMLInputElement> document.getElementById("age");
        //<HTMLInputElement> document.getElementById

        var CheckM = userList.length
          && userList.some((userObj:any)=> 
            userObj.userName.toLowerCase() == dupMname.value || userObj.userName.toUpperCase() == dupMname.value
            && userObj.userAge == dupMage.value)
          
          if(!CheckM) {
            userList.push(this.userObj);
            userList.sort((a:any, b:any) => parseFloat(a.userAge) - parseFloat(b.userAge));
            localStorage.setItem('userList', JSON.stringify(userList));

            this.router.navigateByUrl('/userList');
          }else {
            return alert('find Duplicate');
          }
      }
    }

    else if(this.userObj.usergender === false){
      debugger;
      const latesntId = this.getNewnUserId();
      this.userObj.userId = latesntId;
      const oldnRecords = localStorage.getItem('usernList');
      //=>Validation
      this.submitted = true;

      if (this.form.invalid) {
        return;
      }

      //Localstroage
      if (oldnRecords !== null){
        const usernList = JSON.parse(oldnRecords);
        const dupFname = <HTMLInputElement> document.getElementById("name");
        const dupFage = <HTMLInputElement> document.getElementById("age");  //<HTMLInputElement> document.getElementById

        var CheckF = usernList.length 
        && usernList.some((userObj:any)=> 
          userObj.userName == dupFname.value 
          && userObj.userAge == dupFage.value)

        if(!CheckF) {
          usernList.push(this.userObj);
          usernList.sort((a:any, b:any) => parseFloat(b.userAge) - parseFloat(a.userAge));
          localStorage.setItem('usernList', JSON.stringify(usernList));

          this.router.navigateByUrl('/userList');
        }
        else {
          return alert('find Duplicate');
        }
      }
    }

    else if(this.userObj.usergender !== true || this.userObj.usergender !== false){
      this.submitted = true;

    }
  
  }
  onSubmit(): void {
  }

  //Forms
  onReset(): void {
    this.submitted = false;
    this.form.reset();
  }

}