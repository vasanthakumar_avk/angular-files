import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { userObj } from 'src/app/user';
import { AbstractControl, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  userObj: userObj;
  //Validation
  form: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.maxLength(20),Validators.minLength(3), Validators.pattern('^\s*([a-zA-Z]+\s?\.?[a-zA-Z]+)+$')]),
    userage: new FormControl('', [Validators.required, Validators.maxLength(3), Validators.minLength(2), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    usergender: new FormControl('', [Validators.required]),
    
  });
  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) {
    this.userObj = new userObj();
    this.route.params.subscribe((res)=> {
      this.userObj.userId = res['id']
    });
  }

  ngOnInit(): void {
    const oldRecords = localStorage.getItem('userList');
    const oldnRecords = localStorage.getItem('usernList');

    if (oldRecords !== null){
      const userList = JSON.parse(oldRecords);
      const currentUser = userList.find((m: any) => m.userId == this.userObj.userId);
      if (currentUser !== undefined) {
        this.userObj.userAge = currentUser.userAge;
        this.userObj.userName = currentUser.userName;
        this.userObj.usergender = currentUser.usergender;
      }
    }
    if (oldnRecords !== null){
      const usernList = JSON.parse(oldnRecords);
      const currentUsers = usernList.find((m: any) => m.userId == this.userObj.userId);
      if (currentUsers !== undefined) {
        this.userObj.userAge = currentUsers.userAge;
        this.userObj.userName = currentUsers.userName;
        this.userObj.usergender = currentUsers.usergender;
      }
    }
  }

  //Validation
  get username() {
    return this.form.get('username');
  }
  get userage() {
    return this.form.get('userage');
  }
  get usergender() {
    return this.form.get('usergender');
  }

  updateUser() {
    const gendermale = (<HTMLInputElement>document.getElementById('male')).value;
    const genderfemale = (<HTMLInputElement>document.getElementById('female')).value;

    if(localStorage.getItem("userList") === null) {
      localStorage.setItem('userList', '[]');
    }
    if(localStorage.getItem("usernList") === null) {
      localStorage.setItem('usernList', '[]');
    }

    if(this.userObj.usergender === true){
      debugger;
      const oldRecords = localStorage.getItem('userList');

      //Validation
      this.submitted = true;

      if (this.form.invalid) {
        return;
      }
      //Localstroage
      if (oldRecords !== null){
        const userList = JSON.parse(oldRecords);
        userList.splice(userList.findIndex((a: any)=>a.userId == this.userObj.userId),1); 

        const dupMname = <HTMLInputElement> document.getElementById("name");
        const dupMage = <HTMLInputElement> document.getElementById("age");
        //<HTMLInputElement> document.getElementById

        var CheckM = userList.length
          && userList.some((userObj:any)=> 
            userObj.userName == dupMname.value 
            && userObj.userAge == dupMage.value)
          
          if(!CheckM) {
            userList.push(this.userObj);
            userList.sort((a:any, b:any) => parseFloat(a.userAge) - parseFloat(b.userAge));
            localStorage.setItem('userList', JSON.stringify(userList));
          }
          else {
            return alert('find Duplicate');
          }
          this.router.navigateByUrl('/userList');
      }
        
    }
    else if (this.userObj.usergender === false){
      debugger;
      const oldnRecords = localStorage.getItem('usernList');

      //Validation
      this.submitted = true;

      if (this.form.invalid) {
        return;
      }
      //Localstroage
      if (oldnRecords !== null){
        const usernList = JSON.parse(oldnRecords);
        usernList.splice(usernList.findIndex((b: any)=>b.userId == this.userObj.userId),1);

        const dupFname = <HTMLInputElement> document.getElementById("name");
        const dupFage = <HTMLInputElement> document.getElementById("age");  
        //<HTMLInputElement> document.getElementById


        var CheckF = usernList.length 
        && usernList.some((userObj:any)=> 
          userObj.userName == dupFname.value 
          && userObj.userAge == dupFage.value)

        if(!CheckF) {

          usernList.push(this.userObj);
          usernList.sort((a:any, b:any) => parseFloat(b.userAge) - parseFloat(a.userAge));
          localStorage.setItem('usernList', JSON.stringify(usernList));

        }
          else {
            return alert('find Duplicate');
          }
           this.router.navigateByUrl('/userList');
      }
          
    }
    else if(this.userObj.usergender !== true || this.userObj.usergender !== false){
    this.submitted = true;

  }else {}
  }
  onSubmit(): void {
  }
  //Forms
  onReset(): void {
    this.submitted = false;
    this.form.reset();
  }

}
