import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsernListComponent } from './usern-list.component';

describe('UsernListComponent', () => {
  let component: UsernListComponent;
  let fixture: ComponentFixture<UsernListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsernListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsernListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
