import { Component, OnInit } from '@angular/core';
import { userObj } from 'src/app/user';

@Component({
  selector: 'app-usern-list',
  templateUrl: './usern-list.component.html',
  styleUrls: ['./usern-list.component.css']
})
export class UsernListComponent implements OnInit {

  usernList: userObj [];
  Today = new Date();

  constructor() {
    this.usernList = [];
  }

  ngOnInit(): void {
    debugger;
    const recordsn = localStorage.getItem('usernList');
    if (recordsn !== null){
      this.usernList = JSON.parse(recordsn);
    }
  }
  delete(id: any) {
    debugger;
    if(confirm("Are you sure to delete the value ")) {
    const oldnRecords = localStorage.getItem('usernList');
      if (oldnRecords !== null){
        const usernList = JSON.parse(oldnRecords);
        usernList.splice(usernList.findIndex((a: any)=>a.userId == id),1);
        localStorage.setItem('usernList', JSON.stringify(usernList));
        }
      const recordsn = localStorage.getItem('usernList');
      if (recordsn !== null){
        this.usernList = JSON.parse(recordsn);
      }
    }
  }
  reFltb() {
    localStorage.removeItem('usernList');
  }

}
