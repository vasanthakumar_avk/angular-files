import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './components/userApp/user-list/user-list.component';
import { UserRegisterComponent } from './components/userApp/user-register/user-register.component';
import { UserUpdateComponent } from './components/userApp/user-update/user-update.component';

import {MatRadioModule} from '@angular/material/radio';
import {MatInputModule} from '@angular/material/input';
import { UsernListComponent } from './components/userApp/usern-list/usern-list.component';
import { UserSortPipe } from './components/userApp/user-sort.pipe';

const routeArray = [
  {
    path: 'userList',
    component: UserListComponent
  },
  {
    path: 'usernList',
    component: UsernListComponent
  },
  {
    path: 'addUser',
    component: UserRegisterComponent
  },
  {
    path: 'updateUser/:id',
    component: UserUpdateComponent
  },
  { path: '',   redirectTo: '/addUser', pathMatch: 'full' },
];

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,ReactiveFormsModule,
    MatRadioModule,MatInputModule,
    RouterModule.forRoot(routeArray)
  ],
  declarations: [
    AppComponent,
    UserListComponent,
    UserRegisterComponent,
    UserUpdateComponent,
    UsernListComponent,
    UserSortPipe
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
