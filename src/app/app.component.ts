import { Component } from '@angular/core';


@Component({ //decorator & used to convert normal Typescript to Angular Component
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'User Resigration :';
}
